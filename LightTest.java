
import lejos.nxt.*;
import lejos.robotics.navigation.*;

public class LightTest {

	static LightSensor lsense;
	static boolean end;

	public static void main(String[]args) throws Exception {

		lsense = new LightSensor(SensorPort.S2);
		end = true;

		while (end){
			if (lsense.getNormalizedLightValue() > 435) {
					MotorPort.B.controlMotor(50, 1);
					MotorPort.C.controlMotor(0, 3);
					checkColor();
				} else {
					MotorPort.C.controlMotor(50, 1);
					MotorPort.B.controlMotor(0, 3);
					checkColor();
				}
			Thread.sleep(10);
		}

	}

	public static void checkColor() throws Exception {
		int color = lsense.getNormalizedLightValue();

		if (color == 359){
			end = false;
		}
	}

}