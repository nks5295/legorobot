
/*
 * Maze Solver Code
 * Neel Shah, Abby Shriver, Akbar Sattar, Madison Throne, Sean Bird
 *
 * This program utilizes the LeJOS API to make a Lego MindStorms Robot
 * solve a maze.
 */

import lejos.nxt.*;
import lejos.robotics.navigation.*;
import lejos.robotics.Color.*;

public class MazeSolverD {

	//the following are class variables
	static ColorSensor csense;
	static DifferentialPilot pilot;
	static UltrasonicSensor usense;
	static TouchSensor tsense;

	static int currentColor;

	/*
	 * Loops through a while loop that ends whenever the color is green
	 * moves forward, pauses, it displays the color values and light values
	 * on the LCD, it then determines what color is on the board, then it checks
	 * for color again. Whenever it breaks the loop, it has reached green so it stops,
	 * and makes a sound.
	 */
	public static void main(String[]args){

		pilot = new DifferentialPilot(5.6f, 5.6f, 1.5f, Motor.B, Motor.C, false);
		csense = new ColorSensor(SensorPort.S2);

		currentColor = csense.getColor().getColor();
		while (csense.getColor().getColor() != 1){
			moveForward();
			safeSleep(100);
			LCD.drawInt(csense.getColor().getColor(), 4, 0, 0);
			LCD.drawInt(csense.getNormalizedLightValue(), 4, 0, 1);
			checkForColor();
			currentColor = csense.getColor().getColor();
		}
		Sound.beepSequenceUp();

	}

	/*
	 * This method moves the robot forward at a pace
	 * that will allow it to scan all of the colors
	 * as it moves.
	 *
	 * If the touch sensor is pushed, it will do a 180 turn.
	 */
	public static void moveForward(){
		pilot.setTravelSpeed(9); //travel speed is 9 diameters of the wheels
		pilot.travel(3); //travels 3 cm
		safeSleep(100);
		tsense = new TouchSensor(SensorPort.S4);
		if (tsense.isPressed()){
			pilot.steer(200, -1500); //-1500 is a scaled value which represents 180
		}
	}

	/*
	 * This method will determine if the color on the board
	 * is either blue, red, or white. Depending on what color it is,
	 * it will call the appropriate method to decide what to do.
	 */
	public static void checkForColor(){
		if (currentColor == csense.BLUE){
			doBlueStuff();
			safeSleep(100);
		} else if (csense.getNormalizedLightValue() > 380 && csense.getNormalizedLightValue() < 425){ //the numbers are the light value of red
			doRedStuff();
			safeSleep(100);
		} else if (csense.getNormalizedLightValue() > 500 && csense.getNormalizedLightValue() < 550){ //the numbers are the light value of white
			doWhiteStuff();
		}else {
			return;
		}
	}

	/*
	 * This method rotates the ultrasensor 90 degrees left and right
	 * and it determines the distance of the bot to the wall straight, left
	 * and right. Then, it calls a helper method.
	 */
	public static void doBlueStuff(){
		pilot.stop();
		usense = new UltrasonicSensor(SensorPort.S1);
		int straight = usense.getDistance();
		usense = new UltrasonicSensor(SensorPort.S1);
		Motor.A.rotate(90);
		int left = usense.getDistance();
		safeSleep(100);
		usense = new UltrasonicSensor(SensorPort.S1);
		Motor.A.rotate(-90);
		Motor.A.rotate(-90);
		int right = usense.getDistance();
		safeSleep(100);
		Motor.A.rotate(90);

		compareAndMove(straight, left, right);
	}

	/*
	 * This is the helper method of the blue's method. It compares the distance 
	 * of each of the 3 sides and determines which direction to turn.
	 */ 
	public static void compareAndMove(int straight, int left, int right){
		if (right > 20){
			pilot.setTravelSpeed(2); //travel speed is 2 diameters
			pilot.travel(5); //travels 5cm
			pilot.steer(200, 750); //750 is a scaled value of 90 degrees for the turn
		}else if (right > straight && right>left){
			pilot.setTravelSpeed(2); //travel speed is 2 diameters
			pilot.travel(5); //travel 5cm
			pilot.steer(200, 750); //750 is a scaled value of 90 degrees for the turn
		} else if (straight > right && straight > left){
			pilot.setTravelSpeed(3); //travel speed is 3 diameters
			pilot.travel(3); //travel 3cm
		} else if (left > right && left > straight){
			pilot.setTravelSpeed(2); //travel speed is 2 diameters
			pilot.travel(5); //travel 5cm
			pilot.steer(200, -750); //750 is a scaled value of 90 degrees for the turn
		} else {
			pilot.steer(200, 900);
		}
	}

	/*
	 * This method corrects the robots position on the board
	 * if it leaves the black line by shifting it left, then right
	 * until it is back on th black line.
	 */
	public static void doWhiteStuff(){
		pilot.stop();
		for (int i = 0; i < 10; i++){
			pilot.steer(200, 30); //steer 30 degrees to get to the black line
			safeSleep(100);
			if (csense.getColor().getColor() == 7){ //7 is the representation for black
				return;
			}
		}
		for (int i = 0; i < 20; i++){
			pilot.steer(200, -30); //steer 30 degrees to get to the black line
			safeSleep(100);
			if (csense.getColor().getColor() == 7){ //7 is representation for black
				return;
			}
		}
	}

	/*
	 * This method makes the robot beep and then do a 180 turn once it reaches the red.
	 */
	public static void doRedStuff(){
		Sound.beepSequence();
		pilot.steer(200, 1500); //1500 is the scaled value of 180 degrees
	}
	
	/*
	 * This method is a safe way to use Thread.sleep(int ms)
	 * that prevents all exceptions with a try catch.
	 */
	public static void safeSleep(int ms){
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e){
			e.printStackTrace();
		}
	}

}